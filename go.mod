module gitlab.com/abwandners/go-upnp

go 1.14

require (
	gitlab.com/NebulousLabs/fastrand v0.0.0-20181126182046-603482d69e40
	gitlab.com/NebulousLabs/go-upnp v0.0.0-20181011194642-3a71999ed0d3
	golang.org/x/net v0.0.0-20201110031124-69a78807bb2b
)

replace gitlab.com/NebulousLabs/go-upnp => ./
